<?php
/**
 * index.php des Templates "AnimalShelter"
 *
 * @author            Thomas Wenzel
 * @license           GPL
 */
 
defined('_JEXEC') or die;

JHTML::_('behavior.framework', true);
$app = JFactory::getApplication();
?>

<?php echo '<!DOCTYPE html>'; ?>
<!--[if IE 7 ]> <html class="ie7" lang="DE-de"> <![endif]-->
<!--[if IE 8 ]> <html class="ie8" lang="DE-de"> <![endif]-->
<!--[if IE 9 ]> <html class="ie9" lang="DE-de"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
 <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" > 
 <!--<![endif]-->

<head>
        <jdoc:include type="head" />
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/styles.css" type="text/css" />
</head>

<?php 
    $active = JFactory::getApplication()->getMenu()->getActive();
?>
<body class="<?php echo $active->alias; ?> ">



<div class="wrapper"><!--bekommt den seitlichen Schatten-->
      
    <!--Start header-->
    <div id="header">
        <div id="header_left">
            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/logo_headerleft.jpg" alt="header_logo">
        </div>
        <div id="header_right">
            <a href="http://www.tierheim-lindau.de/cms/" title="Zur Startseite">
                <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/logo.png" alt="header_text">
            </a>
        </div>
    </div>
    <!--End header-->

    <!--Start main menu-->
    <div id="mainmenu">
        <jdoc:include type="modules" name="navigation" style="none" />
    </div>
    <!--End main menu-->
        
        
    <?php if(($active->alias)=='home') { ?>
        <div id="col1_3"><!--der linke Inhaltsbereich-->
        
            <div id="navipath"><!--Bereich für die Darstellung der Breadcrumbs-->
                <jdoc:include type="modules" name="breadcrumbs" style="none" />
            </div><!--navipath-->
            
            <div id="slideshow"><!--Bereich für die zufaellige Anzeige von Tieren-->
                <jdoc:include type="modules" name="slideshow" style="none" />
            </div><!--slideshow-->
            
                    <div id="welcome">
          <div class="highlight">
            <h1>Herzlich Willkommen!</h1>
          </div>
          <!--highlight-->
          <p>Schön, dass Sie uns gefunden haben und herzlich willkommen. Auf den folgenden Seiten wollen wir Ihnen unser Tierheim
          und den Tierschutzverein vorstellen.</p>
          <p>Haben Sie Fragen, Wünsche oder Anregungen?
          <br />Sie erreichen uns telefonisch täglich zwischen 15:00 und 18:00 Uhr unter 
          <b>08382 / 72365</b> oder schreiben Sie uns: 
          <a href="mailto:info@tierheim-lindau.de">info@tierheim-lindau.de</a>.</p>
        </div>
        <!--welcome-->
        
        <div id="component">
            <jdoc:include type="component" style="xhtml"/>
        </div><!--#component-->
        
        </div><!--col1_3-->
        
    <?php } ?>
    
    <?php if(($active->alias)!='home') { ?>
        <div id="col1_3"><!--der linke Inhaltsbereich-->
          <div id="navipath"><!--Bereich für die Darstellung der Breadcrumbs-->
                <jdoc:include type="modules" name="breadcrumbs" style="none" />
          </div><!--navipath-->
          <div id="col1">
            <div id="submenu">
              <jdoc:include type="modules" name="navigation_sub" style="none" />
            </div><!---submenu-->
          </div><!---col1-->
          <div id="content">
            <div id="component">
                <jdoc:include type="component" style="xhtml"/>
            </div><!--#component-->
          </div><!--content-->
        </div><!--col1_3-->
    <?php } ?>
    
    <div id="col4"><!--Bereich mit statischem Inhalt auf der rechten Seite-->
            
            <div id="search_area"><!--Volltextsuche-->
                <jdoc:include type="modules" name="search" style="none" />
            </div><!--search_area-->
            
        <div class="extra_links">
          <a href="<?php echo $this->baseurl ?>/index.php/unterstuetzen/mitglied-werden" class="tagIcon_red">Werde Mitglied</a> 
          <a href="<?php echo $this->baseurl ?>/index.php/unterstuetzen/spenden" class="tagIcon_red">Spende jetzt</a> 
          <a href="<?php echo $this->baseurl ?>/index.php/unterstuetzen/patenschaft" class="tagIcon_red">Werde
        Tierpate</a></div>
        <!--extra_links-->
        <div class="float_left indent_left indent_right">
          <p>Tierschutzverein e.V. Stadt- und Landkreis Lindau/Bodensee
          <br />Fraunhoferstraße 40
          <br />88131 Lindau/Bodensee</p>
          <p>Telefon: +49 (0)8382/72365
          <br />
          <a href="mailto:info@tierheim-lindau.de">info@tierheim-lindau.de</a></p>
          <p>
            <a href="<?php echo $this->baseurl ?>/index.php?option=com_content&view=article&id=1">Impressum</a>
          </p>
        </div>
        <div class="extra_links">
            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/dog_left.gif" height="50px" align="right" alt="icon_dog">
            <a href="https://www.facebook.com/tierheimtierschutzvereinlindau.lindau?fref=nf" class="tagIcon_green">facebook</a> 
            <a href="https://www.youtube.com/channel/UC36O8bt6ozGyRJM0fGKRnBw" class="tagIcon_green">youtube</a>
            <a href="<?php echo $this->baseurl ?>/index.php/aktuelles-und-service/newsletter" class="tagIcon_green">Newsletter</a>
        </div>
        <!--extra_links-->
          
        <div class="float_left indent_left indent_right">
            <jdoc:include type="modules" name="opening_time" style="none" />
        </div>
    </div><!--col4-->

    <div class="clear"></div>

    <footer>
        <div class="splitter"></div>
            <!--start bar-->
            <div class="bar">
                
            <!--start copyright-->
			<div class="copyright">&copy; 2014–2017 built with ❤️</div>
            <!--end copyright-->

        </div>
        <!--end bar-->

        </div>

    </footer>
    
</div><!--wrapper-->

</body>
</html>

